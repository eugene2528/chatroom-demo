//check whether TEST class existed
var TEST = TEST || {};
var USERNAME = null;

$(function(){
	//collection of all api ajax call, easier to manage and give more flexibility
	TEST.apiMethods = {
		setName : function(info,callback) {
			return $.post("api/controller.php",{
				name : info.name
			}).done(function(data){
				if( data.rtnCode < 0 )
					throw "Error posting msgs : " + data.rtnMsg;
				callback && callback(data.data);
			});
		},
		getMsg : function(callback) {
			return $.get("api/controller.php",function(data){
				if( data.rtnCode < 0 )
					throw "Error loading msgs : " + data.rtnMsg;
				callback && callback(data.data);
			});
		},
		postMsg : function(info,callback) {
			return $.post("api/controller.php",{
				text : info.text
			}).done(function(data){
				if( data.rtnCode < 0 )
					throw "Error posting msgs : " + data.rtnMsg;
				callback && callback(data.data);
			});
		},
		likeMsg : function(info,callback) {
			return $.post("api/controller.php",{
				like : info.msgId
			}).done(function(data){
				if( data.rtnCode < 0 )
					throw "Error to like msg : " + data.rtnMsg;
				callback && callback(data.data);
			});
		}
	};
});

//MsgObject
$(function(){
	TEST.MsgObject = function(config){
		//constructor
		this.data = null;
		this.parentObj = null;
		this.DOM = null;
		this._primaryKey = null;
		init.call(this,config);
	};
	TEST.MsgObject.prototype = {
		like : function(){
			//like a msg
			var _root = this;
			TEST.apiMethods.likeMsg({ msgId : _root.id },function(data){
				_root.data.like_cnt++;
				_root.update();
			});
		},
		update : function(){
			//update the content within the DOM
			var $targetDom = this.DOM;
			$targetDom.data('TESTobj',this);
			$targetDom.attr('id',this.data[this._primaryKey]);
			$.each(this.data,function(key,value){
				$targetDom.find('[data-binding='+key+']').text(value);
			});

		}
	};
	Object.defineProperties(TEST.MsgObject.prototype,{
		id : {
			//set the property for id
			set : function(val) {
				// do nothing
			},
			get : function(){
				return this.data[this._primaryKey];
			}
		}
	});
	function init(config){
		//initizialize for the msg object
		if( !( config instanceof Object ) )
			throw "Config is not an object";
		this.data = config.data;
		this.parentObj = config.parent;
		this._primaryKey = config.primaryKey;
		this.DOM = this.parentObj.template.clone(true,true).removeClass('template');
		this.update();
	}
});

//Msg Container Obj
$(function(){
	TEST.MsgContainer = function(config) {
		//constructor
		this._containList = [];
		this.DOM = null;
		this.template = null;
		this._childPrimaryKey = null;
		this.requestState = 0;
		this.jqRequest = null;
		init.call(this,config);
	}
	TEST.MsgContainer.prototype = {
		autoUpdate : function(){
			//method for starting auto update
			var _root = this;
			var routin = setInterval(function(){
				if( _root.requestState == -1 ) // for stop auto update
					clearInterval(routin);
				else if( _root.requestState == 0 ) { // no other request running
					_root.requestState = 1;
					_root.getMsg(function(){
						_root.requestState = 0;
					});
				} 
			},1000);
		},
		getMsg : function(callback){
			var _root = this;
			TEST.apiMethods.getMsg(function(data){
				$.each(data,function(ind,value){
					var getObj = _root.getElementById(value[_root._childPrimaryKey]);
					if( getObj == null ) { // to see whether the object is existed
						//if not exist then create
						var newMsg = _root.createMsg(value);
						_root.append(newMsg);
					}
					else {
						//if exist then update
						getObj.data = value;
						getObj.update();
					}
				});
				window.scrollTo(0,document.body.scrollHeight);
				callback && callback(); // call the callback function
			});
		},
		getElementById : function(targetId){
			//to find specific object
			var _root = this;
			var result = null;
			$.each(_root._containList,function(key,object){
				if( object.id == targetId ) {
					result = object;
					return false;
				}
			});
			return result;
		},
		createMsg : function(data){
			//create msg object
			var _root = this;
			return new TEST.MsgObject({
				data : data,
				parent : _root,
				primaryKey : _root._childPrimaryKey
			});
		},
		append : function(msgObj){
			//append the msg dom to the dom container
			//changing the view
			if( !(msgObj instanceof TEST.MsgObject ) )
				throw "Illegel msg object.";
			this._containList.push(msgObj);
			this.DOM.append( msgObj.DOM );
		},
		postMsg : function(text){
			//posting a msg
			var _root = this;
			TEST.apiMethods.postMsg({text:text},function(data){
				var newMsg = _root.createMsg(data);
				_root.append(newMsg);
				window.scrollTo(0,document.body.scrollHeight);
			})
		}
	};
	function init(config) {
		if( !( config instanceof Object ) )
			throw "Config is not an object";
		this.DOM = $(config.containerDOM);
		this.template = this.DOM.find('>.template');
		this._childPrimaryKey = config.primaryKey;
	}
	$.fn.TESTMsg = function(config){
		//create a jQuery extension
		config = config || {};
		config.containerDOM = this;
		return new TEST.MsgContainer(config);
	};
})

//set a global variable for the TEST object
var MAIN = null;
$(function(){
	
	$('.input_container .input').bind('keyup, keypress',function(e){
		if( e.which == 13 )
			$('.input_container .submit').trigger('click');
	});
	$('.input_container .submit').click(function(){
		var inputString = $('.input_container .input').val();
		if( USERNAME == null ) {
			//when inputing name
			USERNAME = inputString;
			$('.input_container .current_name').text( inputString );
			$('.input_container .input').attr('placeholder',$('.input_container .input').attr('placeholder-inputmsg') )
			
			//sending set name api
			TEST.apiMethods.setName({name:inputString},function(){
				//creating TEST object
				MAIN = $('.msg_container').TESTMsg({primaryKey:"tid"});
				MAIN.autoUpdate();
			});
		}
		else {
			//when posting msg
			MAIN.postMsg(inputString);
		}
		$('.input_container .input').val('');
	});
	$('.msg.template .like_cnt').click(function(){
		//to trigger the like method
		$(this).parent('.msg').data('TESTobj').like();
	})
});