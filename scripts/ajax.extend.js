jQuery.each( [ "put", "delete" ], function( i, method ) {
  jQuery[ method ] = function( url, data, callback, type ) {
    if ( jQuery.isFunction( data ) ) {
      type = type || callback;
      callback = data;
      data = undefined;
    }
	var data = data || {};
	var met = (method=="put")?"PUT":"DELETE";
    return jQuery.ajax({
		headers: {
			'X-HTTP-Method-Override': met
		},
		url: url+(data.id?'?id='+data.id:''),
		type: met,
		dataType: type,
		data: data,
		success: callback
    });
  };
});