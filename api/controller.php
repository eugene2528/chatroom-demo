<?php
session_start();
header('Content-Type: application/json');;

$host = "localhost";
$user = "root";
$password = "eugene";
$database = "web_teach";

// initialize PDO
$dbConnection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
$dbConnection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$rtnObj = array("rtnCode"=>0,"rtnMsg"=>"");

if( $_SERVER['REQUEST_METHOD'] == "GET" ) // get
{
	//get all msgs
	$request = $dbConnection -> query("select * from text order by tsp desc limit 20;");
	$request -> setFetchMode(PDO::FETCH_ASSOC);
	$data = $request->fetchAll();

	$rtnObj["data"] = array_reverse($data);
	$rtnObj["rtnCode"] = 1000;
	$rtnObj["rtnMsg"] = "Get data successfully.";
}
else if( $_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['name']) )
{
	//set username
	$_SESSION['test.username'] = mysql_real_escape_string( $_POST['name'] );

	$rtnObj['rtnCode'] = 2000;
	$rtnObj['rtnMsg'] = "Set user name successfully.";
}
else if( $_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['text']) ) 
{
	//post msg
	$postText = mysql_real_escape_string( $_POST['text'] );
	$postUser = $_SESSION['test.username'];

	if( !empty($_SERVER['HTTP_CLIENT_IP']) ) 
		$postIP = $_SERVER['HTTP_CLIENT_IP'];
	else if( !empty($_SERVER['HTTP_X_FORWARDED_FOR']) )
		$postIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else {
		$postIP = $_SERVER['REMOTE_ADDR'];
	}

	$request = $dbConnection -> prepare("insert into text (username,text,ip) values (?,?,?);");

	$request -> execute( array($postUser,$postText,$postIP) );
	$request -> setFetchMode(PDO::FETCH_ASSOC);
	$data = $request->fetchAll();

	$rtnObj["data"] = array(
		"tid" => ($dbConnection->lastInsertId()),
		"username" => $_SESSION['test.username'],
		"text" => $postText,
		"ip" => $postIP,
		"tsp" => (new DateTime('NOW'))->format('Y-m-d H:i:s')
	);
	$rtnObj["rtnCode"] = 3000;
	$rtnObj["rtnMsg"] = "Post msg successfully.";
}
else if( $_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['like']) ) 
{
	//like a msg
	$likeId = (int)$_POST['like'];

	$request = $dbConnection -> prepare("update text set like_cnt=like_cnt+1 where tid=?;");
	$request -> execute( array($likeId) );

	$rtnObj["rtnCode"] = 4000;
	$rtnObj["rtnMsg"] = "Like msg successfully.";
}

echo json_encode($rtnObj);

?>